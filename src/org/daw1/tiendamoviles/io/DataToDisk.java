/*
 * Copyright 2022 rgcenteno.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.daw1.tiendamoviles.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.daw1.tiendamoviles.clases.Tienda;
/**
 * 
 *
 * @author rgcenteno
 */
public class DataToDisk {
    
    private static final String DATA_FOLDER_STR = "."+ File.separator +"data";
    private static final String FILE_NAME = "tienda.dat";
    private static final File DATA_FOLDER = new File(Paths.get(DATA_FOLDER_STR).normalize().toString());
    private static final File DATA_FILE = new File(Paths.get(DATA_FOLDER + File.separator + FILE_NAME).normalize().toString());
       
    public static boolean saveTienda(Tienda tienda){
      
        if(!DATA_FOLDER.exists()){
            DATA_FOLDER.mkdirs();
        }
                
        if(!DATA_FILE.exists()){
            try {
                DATA_FILE.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(DataToDisk.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(DATA_FILE.exists() && DATA_FILE.canWrite()){
            try(OutputStream out = new FileOutputStream(DATA_FILE);
                ObjectOutputStream outObj = new ObjectOutputStream(new BufferedOutputStream(out))){                
                outObj.writeObject(tienda);                
                return true;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(DataToDisk.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(DataToDisk.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
            Logger.getLogger(DataToDisk.class.getName()).log(Level.SEVERE, "Sin permisos de escritura en el fichero ", DATA_FILE);
        }
        return false;
    }
    
    public static Tienda loadTienda(){                        
        if(DATA_FILE.exists()){
            try (InputStream in = new FileInputStream(DATA_FILE);
                ObjectInputStream objIn = new ObjectInputStream(new BufferedInputStream(in))){                    
                Tienda tienda = (Tienda)objIn.readObject();
                return tienda;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(DataToDisk.class.getName()).log(Level.SEVERE, null, ex);
            }
            catch(java.io.IOException | ClassNotFoundException ex){
                Logger.getLogger(DataToDisk.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
            Logger.getLogger(DataToDisk.class.getName()).log(Level.INFO, "fichero de datos no encontrado", DATA_FILE);
        }
        return null;
    }
    
    public static boolean existsDataFile(){        
        return (DATA_FILE.exists() && DATA_FILE.canRead());
    }
}
