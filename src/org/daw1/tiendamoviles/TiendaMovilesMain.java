/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package org.daw1.tiendamoviles;

import java.time.LocalDate;
import java.util.List;
import java.util.regex.Pattern;
import org.daw1.tiendamoviles.clases.Movil;
import org.daw1.tiendamoviles.clases.SistemaOperativo;
import org.daw1.tiendamoviles.clases.Tienda;
import org.daw1.tiendamoviles.io.DataToDisk;

/**
 *
 * @author Rafael González Centeno
 */
public class TiendaMovilesMain {

    private static java.util.Scanner teclado;
    private static Tienda tienda;
    
    public static void main(String[] args) {
        teclado = new java.util.Scanner(System.in);                
        if(DataToDisk.existsDataFile()){
            String resp = "";
            do{
                System.out.println("Se ha detectado un archivo de guardado. ¿Desea cargarlo? (s)i, (n)o");
                resp = teclado.nextLine();
                if(resp.equalsIgnoreCase("s")){
                    loadTienda();                    
                }
            }
            while(!resp.equalsIgnoreCase("s") && !resp.equalsIgnoreCase("n"));            
        }
        if(tienda == null){
            String nombreTienda = getStringNotBlank("Inserte el nombre de la tienda", "El nombre no puede estar en blanco y debe estar formado por letras, números espacios y puntos", Tienda.PATRON_NOMBRE);
            String cif = getStringNotBlank("Inserte el cif de la tienda", "El cif debe seguir el formato A12345678", Tienda.PATRON_CIF);       
            tienda = new Tienda(cif, nombreTienda);
        }
        String opcion;
        do{
            System.out.println("*************************************************************");
            System.out.println("* 1. Alta móvil                                             *");
            System.out.println("* 2. Registrar venta móvil                                  *");
            System.out.println("* 3. Mostrar número e importe de móviles no vendidos        *");
            System.out.println("* 4. Mostrar número e importe de móviles vendidos           *");
            System.out.println("* 5. Mostrar listado de móviles no vendidos                 *");
            System.out.println("* 6. Mostrar listado de móviles vendidos                    *");
            System.out.println("* 7. Guardar datos a disco                                  *");
            System.out.println("* 8. Cargar datos de disco                                  *");
            System.out.println("*                                                           *");
            System.out.println("* 0. Salir                                                  *");
            System.out.println("*************************************************************");
            opcion = teclado.nextLine();
            switch(opcion){
                case "1":
                    Movil m = crearMovil();
                    if(tienda.addMovil(m)){
                        System.out.println("Móvil añadido correctamente");
                    }
                    else{
                        System.out.println("El móvil no se ha añadido a la tienda");
                    }
                    break;
                case "2":
                    System.out.println("Inserte el imei del móvil a vender");
                    String imei = teclado.nextLine();
                    m = tienda.buscarMovilByImei(imei);
                    if(m == null){
                        System.out.println("El móvil solicitado no existe");
                    }
                    else{
                        if(m.isVendido()){
                            System.out.println("El móvil ya se ha vendido con anterioridad");
                        }
                        else{
                            m.marcarComoVendido();
                        }
                    }
                    break;
                case "3":
                    System.out.println("Teléfonos no vendidos: " + tienda.contarVendidos(false) + ". PVP total: " + tienda.pvpVendidos(false));
                    break;
                case "4":
                    System.out.println("Teléfonos vendidos: " + tienda.contarVendidos(true) + ". PVP total: " + tienda.pvpVendidos(true));
                    //Muestra cuantos móviles están vendidos y el importe total de venta de estos (teniendo en cuenta que el PVP sigue la fórmula del documento)
                    break;
                case "5":
                    List<Movil> lista = tienda.getMovilesVendidos(false);
                    for(Movil movil : lista){
                        System.out.println(movil);
                    }
                    break;
                case "6":
                    lista = tienda.getMovilesVendidos(true);
                    for(Movil movil : lista){
                        System.out.println(movil);
                    }
                    break;
                case "7":
                    if(org.daw1.tiendamoviles.io.DataToDisk.saveTienda(tienda)){
                        System.out.println("Guardado correctamente");
                    }
                    else{
                        System.out.println("Error en el guardado, consulte el registro de logging");
                    }
                    break;
                case "8":
                    loadTienda();
                    break;
                case "0":
                    break;
                default:
                    System.out.println("Opción no válida");
                    break;
            }
        }
        while(!opcion.equals("0"));
    }
    
    public static void loadTienda(){
        Tienda t = org.daw1.tiendamoviles.io.DataToDisk.loadTienda();
        if(t != null){
            tienda = t;
            System.out.println("¡Se han cargado los datos de la tienda " + tienda.toString() + " correctamente!");
        }
        else{                        
            System.out.println("Error en la carga del fichero, consulte el registro de logging");
        }
    }
    
    public static String getStringNotBlank(String textoPregunta, String textoError, Pattern patron){
        String txt = "";
        do{
            System.out.println(textoPregunta);
            txt = teclado.nextLine();
            if(txt.isBlank() || !patron.matcher(txt).matches()){
                System.out.println(textoError);
            }
        }
        while(txt.isBlank() || !patron.matcher(txt).matches());
        return txt;
    }
    
    public static Movil crearMovil(){
        String marca = getStringNotBlank("Inserte la marca del móvil", "Sólo se permiten números y espacios", Movil.PATRON_ALFANUMERICO_ESPACIOS);
        String modelo = getStringNotBlank("Inserte el modelo del móvil", "Sólo se permiten números y espacios", Movil.PATRON_ALFANUMERICO_ESPACIOS);
        String imei = getStringNotBlank("Inserte el IMEI del móvil", "El IMEI debe estar formado por 15 dígitos", Movil.PATRON_IMEI);
        int anhoLanzamiento = 9999;
        do{
            System.out.println("Inserte el año de lanzamiento");
            if(teclado.hasNextInt()){
                anhoLanzamiento = teclado.nextInt();
                if(anhoLanzamiento > LocalDate.now().getYear()){
                    System.out.println("El año no puede ser posterior al actual");
                }
            }
            teclado.nextLine();
        }
        while(anhoLanzamiento > LocalDate.now().getYear());
        
        SistemaOperativo so = null;
        int i = 1;
        for(SistemaOperativo s : SistemaOperativo.values()){
            System.out.println(i + ". " + s);
            i++;
        }
        
        int opcion = -1;
        do{
            System.out.println("Inserte el número asociado al SO");
            if(teclado.hasNextInt()){
                opcion = teclado.nextInt();
                if(opcion < 1 || opcion > SistemaOperativo.values().length){
                    System.out.println("Número inválido");
                }
                else{
                    so = SistemaOperativo.of(opcion);
                }
            }
            teclado.nextLine();
        }
        while(so == null);
        
        double precioCompra = -1;
        do{
            System.out.println("Inserte el precio de compra");
            if(teclado.hasNextDouble()){
                precioCompra = teclado.nextDouble();
                if(precioCompra <= 0){
                    System.out.println("El coste debe ser positivo");
                }                
            }
            teclado.nextLine();
        }
        while(precioCompra <= 0);
        int margen = -1;
        do{
            System.out.println("Inserte el margen de beneficio en porcentaja");
            if(teclado.hasNextInt()){
                margen = teclado.nextInt();
                if(margen < 0){
                    System.out.println("El margen no puede ser negativo");
                }
            }
            teclado.nextLine();
        }
        while(margen < 0);
        
        return new Movil(marca, modelo, imei, anhoLanzamiento, so, precioCompra, margen);
    }
    
}
