/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.tiendamoviles.clases;

/**
 *
 * @author rgcenteno
 */

public enum SistemaOperativo{
    
    ANDROID, IOS, SYMBIAN, OTRO;
    
    public static SistemaOperativo of(int i){
        if(i < 1 || i > SistemaOperativo.values().length){
            throw new IllegalArgumentException("Sólo se permiten valores entre 1 y " + SistemaOperativo.values().length );            
        }
        else{
            return SistemaOperativo.values()[i - 1];
        }
    }
}
