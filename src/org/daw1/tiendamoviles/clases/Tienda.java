/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.tiendamoviles.clases;

import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.regex.Pattern;

/**
 *
 * @author rgcenteno
 */
public class Tienda implements java.io.Serializable{
    
    private final String nombre;
    private final String cif;
    private final Map<String, Movil> moviles;
    
    static final long serialVersionUID = 1L;
    
    public final static java.util.regex.Pattern PATRON_NOMBRE = Pattern.compile("[A-Za-z0-9 \\.]+");
    public final static java.util.regex.Pattern PATRON_CIF = Pattern.compile("[A-Z][0-9]{8}");

    public Tienda(String cif, String nombre) {
        checkNombre(nombre);
        checkCif(cif);
        this.nombre = nombre;
        this.cif = cif;
        this.moviles = new java.util.TreeMap<>();
    }
    
    private static void checkNombre(String nombre){
        if(nombre == null || nombre.isBlank() || !PATRON_NOMBRE.matcher(nombre).matches()){
            throw new IllegalArgumentException("Sólo se permiten letras, números, espacios y puntos. Null y textos en blanco no válidos");
        }
    }
    
    private static void checkCif(String cif){
        if(cif == null || cif.isBlank() || !PATRON_CIF.matcher(cif).matches()){
            throw new IllegalArgumentException("Patrón del CIF: A12345678");
        }
    }
    
    /**
     * Registra un móvil en la tienda
     * @param m Móvil a registrar
     * @return true si se registra, falso si no se registra porque ya existe en la tienda
     */
    public boolean addMovil(Movil m){
        if(moviles.containsKey(m.getImei())){
            return false;
        }
        else{
            moviles.put(m.getImei(), m);
            return true;
        }
    }
    
    /**
     * Devuelve el móvil con el imei pasado como parámetro. Null si no existe el móvil
     * @param imei Imei a buscar
     * @return Móvil con imei pasado como parámetro o null en caso de no existir
     */
    public Movil buscarMovilByImei(String imei){
        if(moviles.containsKey(imei)){
            return moviles.get(imei);
        }
        else{
            return null;
        }
    }
    
    public int contarVendidos(boolean vendidos){
        int encontrados = 0;
        for(Movil m : moviles.values()){
            if(m.isVendido() == vendidos){
                encontrados++;
            }
        }
        return encontrados;
    }    
    
    public double pvpVendidos(boolean vendidos){
        double total = 0;
        for(Movil m : moviles.values()){
            if(m.isVendido() == vendidos){
                total += m.getPvp();
            }
        }
        return total;
    }
    
    public List<Movil> getMovilesVendidos(boolean vendido){
        List<Movil> resultado = new java.util.LinkedList<>();
        for(Movil m : moviles.values()){
            if(m.isVendido() == vendido){
                resultado.add(m);
            }
        }
        return resultado;
    }

    @Override
    public String toString() {
        return nombre + " (" + cif + ')';
    }
    
    
}
