/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.tiendamoviles.clases;

import java.time.LocalDate;
/**
 *
 * @author rgcenteno
 */
public class Movil implements Comparable<Movil>, java.io.Serializable {
    
    static final long serialVersionUID = 2L;
    
    private final String marca;
    private final String modelo;
    private final String imei;
    private final int anhoLanzamiento;
    private final SistemaOperativo sistemaOperativo;
    private final LocalDate fechaCompra;
    private LocalDate fechaVenta;
    private final double precioCompra;
    private int margen;
    private Integer iva;    
    
    private static int ivaGeneral = 21;
    public static final java.util.regex.Pattern PATRON_ALFANUMERICO_ESPACIOS = java.util.regex.Pattern.compile("[a-zA-Z0-9 ]+");
    public static final java.util.regex.Pattern PATRON_IMEI = java.util.regex.Pattern.compile("[0-9]{15}");

    public Movil(String marca, String modelo, String imei, int anhoLanzamiento, SistemaOperativo sistemaOperativo, double precioCompra, int margen) {
        checkMarcaModelo(marca);
        checkMarcaModelo(modelo);
        checkImei(imei);
        checkAnhoLanzamiento(anhoLanzamiento);
        checkSistemaOperativo(sistemaOperativo);
        checkPrecioCompra(precioCompra);
        checkMargen(margen);
        this.marca = marca;
        this.modelo = modelo;
        this.imei = imei;
        this.anhoLanzamiento = anhoLanzamiento;
        this.sistemaOperativo = sistemaOperativo;
        this.fechaCompra = LocalDate.now();
        this.fechaVenta = null;
        this.precioCompra = precioCompra;
        this.margen = margen;
        this.iva = null;
    }
    
    public static void checkMarcaModelo(String txt){
        if(txt == null || txt.isBlank() || !PATRON_ALFANUMERICO_ESPACIOS.matcher(txt).matches()){
            throw new IllegalArgumentException();
        }
    }
    
    public static void checkImei(String imei){
        if(imei == null || imei.isBlank() || !PATRON_IMEI.matcher(imei).matches()){
            throw new IllegalArgumentException();
        }
    }
    
    public static void checkAnhoLanzamiento(int anho){
        if(anho > java.time.LocalDate.now().getYear()){
            throw new IllegalArgumentException();
        }
    }   
    
    private static void checkPrecioCompra(double precio){
        if(precio < 0){
            throw new IllegalArgumentException();
        }
    }
    
    private static void checkMargen(int margen){
        if(margen < 0){
            throw new IllegalArgumentException();
        }
    }
    
    private static void checkSistemaOperativo(SistemaOperativo so){
        if(so == null){
            throw new IllegalArgumentException();
        }
    }
    
    public void marcarComoVendido(){
        if(fechaVenta == null){
            this.fechaVenta = LocalDate.now();
            this.iva = Movil.ivaGeneral;
        }
        else{
            new RuntimeException("No se permite la venta de un móvil dos veces");
        }        
    }

    public String getImei() {
        return imei;
    }
    
    public boolean isVendido(){
        return this.fechaVenta != null;
    }
    
    public double getPvp(){
        if(this.isVendido()){
            return precioCompra * (1 + (margen / 100d)) * (1 + (this.iva/100d));
        }
        else{
            return precioCompra * (1 + (margen / 100d)) * (1 + (Movil.ivaGeneral / 100d));
        }
    }

    @Override
    public String toString() {
        int iva = this.isVendido() ? this.iva : Movil.ivaGeneral;
        String fechaVenta = (this.fechaVenta == null) ? "No se ha vendido" : this.fechaVenta.toString();
        return "IMEI: "+this.imei+"\nMarca: "+this.marca+"\nModelo: "+this.modelo+"\nAño Lanzamiento: "+ this.anhoLanzamiento+"\nSistema Operativo: "+this.sistemaOperativo+"\nFecha de compra: "+this.fechaCompra+"\nFecha de venta: "+fechaVenta+"\nPrecio compra: "+this.precioCompra+"\nMargen: "+this.margen+"\nIVA: "+iva+"%\nPVP: "+this.getPvp()+"€\n";
    }

    
    @Override
    public int compareTo(Movil t) {
        java.util.Objects.requireNonNull(t, "No se puede comparar con valores nulos");
        return this.imei.compareTo(t.imei);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        else if(obj == this){
            return true;
        }
        else if(obj instanceof Movil){
            Movil aux = (Movil)obj;
            return this.imei.equals(aux.imei);
        }
        else{
            return false;
        }
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(this.imei);
    }
    
    
    
    
    
}
