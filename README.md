Queremos crear una aplicación para gestionar una tienda de móviles. La tienda tiene un nombre (letras, números, espacios y puntos no en blanco) y un CIF (1 letra mayúscula y 8 números). La tienda quiere guardar un histórico de los móviles que tiene y ha vendido y de los que tiene y aún están en venta. De cada móvil queremos guardar:

    Marca (Sólo letras y números no vacío)
    Modelo ((Sólo letras y números no vacío)
    IMEI (15 dígitos)
    Año lanzamiento (int)
    Sistema operativo: (Android | iOS | Symbian | Otro)
    Fecha de compra
    Fecha de venta
    Precio compra
    Margen de beneficio (un número mayor que 0 que nos dice el porcentaje que se quiere ganar con la venta). El porcentaje puede ser mayor que 100.
    PVP: Se calcula calculando el neto que sería para un móvil de 1.000 euros el siguiente es decir 1.200 y aplicándole el IVA general. Actualmente es el 21% por lo que el PVP sería: 1.200 x 1,21 = 1.452€. El IVA es común para todos los móviles pero tenemos que tener en cuenta que éste puede variar y no afectar su cambio a los móviles ya vendidos.
    Fecha de venta: No puede ser anterior a fecha de compra ni posterior a hoy. Puede ser null lo cual quiere decir que el móvil no se ha vendido aún. Al crear un móvil ésta siempre será NULL.

Dos móviles son iguales si tienen el mismo IMEI. Los móviles se ordenan de forma natural por IMEI alfabéticamente. Al arrancar el programa nos solicitará el nombre de la tienda, el CIF y el IVA a aplicar a los móviles. Posteriormente mostrará el menú.

    Registrar un móvil, validando los datos insertados. Comprobar que no se inserta un IMEI ya existente. En ese caso se aborta la petición de datos y no se inserta el móvil.
    Registrar venta de móvil. Se solicita el IMEI del móvil a vender. Si éste existe y no está vendido ya, se registrar la venta con fecha de hoy. Debemos almacenar el IVA que se aplicó en el momento de la venta.
    Se muestra el número de móviles no vendidos y el importe PVP total resultado de la suma de los PVPs de cada teléfono.
    Se muestra el número de móviles vendidos y el importe PVP total resultado de la suma de los PVPs de cada teléfono en el momento de la venta.
    Se muestra un listado con los móviles no vendidos. Para mostrar los datos se utiliza System.out.println(movil). Texto preformateado en texto-preformateado.txt.
    Se muestra un listado con los móviles vendidos. Para mostrar los datos se utiliza System.out.println(movil). Texto preformateado en texto-preformateado.txt.
